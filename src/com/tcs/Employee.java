package com.tcs;

public abstract class Employee {
	private int empId;
	private String empName;
	private String workLocation;
	private double basicPay;
	
	public Employee(int empId, String empName, String workLocation,
			double basicPay) {
		this.empId = empId;
		this.empName = empName;
		this.workLocation = workLocation;
		this.basicPay = basicPay;
	}
	public Employee(){
		
	}

	public abstract double getSalary(int noOfDayWork);
	
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getWorkLocation() {
		return workLocation;
	}
	public void setWorkLocation(String workLocation) {
		this.workLocation = workLocation;
	}
	public double getBasicPay() {
		return basicPay;
	}
	public void setBasicPay(double basicPay) {
		this.basicPay = basicPay;
	}
}
