package com.tcs;
import java.util.ArrayList;
import java.util.HashMap;

public class Company {
	private String companyName;
	private ArrayList<Employee> employeeListArray;
	private ArrayList<Project> projectListArray;
	public Company(String companyName) {
		employeeListArray = new ArrayList<Employee>();
		projectListArray = new ArrayList<Project>();
		this.companyName = companyName;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public void addEmployee(int empId, String empName, String workLocation, double basicPay, double hra){
		boolean flag = false;
		for(Employee e: employeeListArray){
			if(e.getEmpId() == empId){
				flag = true;
				break;
			}
		}
		if(!flag)
			employeeListArray.add(new PermanantEmployee(empId, empName, workLocation, basicPay, hra));
		else
			System.out.println("Employee already Exist");
	}
	
	public void addProject(int projectId, String projectName, int duration){
		boolean flag = false;
		for(Project p: projectListArray) {
			if(p.getProjectId() == projectId){
				flag = true;
				break;
			}
		}
		if(flag)
			System.out.println("Project is already exist");
		else
			projectListArray.add(new Project(projectId, projectName, duration));
	}
	public boolean addEmployeeToProject(int projectId, int employeeId) throws EmployeeNotFoundException {
		boolean flag = false;
		Employee selectedEmp = null;
		for(Employee e: employeeListArray){
			if(e.getEmpId() == employeeId){
				selectedEmp = e;
				flag = true;
				break;
			}
		}
		if(flag){
			boolean flag1 = false;
			Project selectedProject = null;
			for(Project p: projectListArray){
				if(p.getProjectId() == projectId){
					selectedProject = p;
					flag1 = true;
					break;
				}
			}
			if(flag1){
				boolean flag2 = false;
				for(Employee e: selectedProject.getEmpList()){
					if(e.getEmpId() == employeeId){
						flag2 = true;
						break;
					}
				}
				if(flag2)
					return false;
				else
					selectedProject.addEmployee(selectedEmp);
			} else
				System.out.println("Project does not exist");
		} else
			throw new EmployeeNotFoundException();
		return false;
	}
	
	public ArrayList<Employee> getEmployeeDetails(int projectId){
		for(Project p: projectListArray){
			if(p.getProjectId() == projectId){
				return p.getEmpList();
			}
		}
		return null;
	}
	
	public Project getProjectDetails(int empId){
		for(Project p : projectListArray){
			for(Employee e : p.getEmpList()){
				if(e.getEmpId() == empId)
					return p;
			}
		}
		return null;
	}
	
	public double totalSalaryOfallEmps(int projectId, int noOfDaysWorked){
		double salary = 0;
		for(Project p : projectListArray){
			for(Employee e : p.getEmpList()){
				if(e instanceof PermanantEmployee){
					PermanantEmployee emp=(PermanantEmployee) e;
					salary += (noOfDaysWorked*emp.getBasicPay())+(emp.getBasicPay()*  emp.getHra())/100;
				}	
				else{
					salary += (noOfDaysWorked*e.getBasicPay())+e.getBasicPay()/100;
				}
					
						
			}
		}
		return salary;
	}
	
	public HashMap<Integer, Integer> empCountMap(){
		HashMap<Integer, Integer> hm = new HashMap<Integer, Integer>();
		for(Project p : projectListArray){
			hm.put(p.getProjectId(), p.getEmpList().size());
		}
		return hm;
	}
	
	
}
