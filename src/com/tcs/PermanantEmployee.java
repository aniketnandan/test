package com.tcs;

public class PermanantEmployee extends Employee {
	private double hra;
	
	public PermanantEmployee(int empId, String empName, String workLocation, double basicPay, double hra) {
		super(empId, empName, workLocation, basicPay);
		this.hra = hra;
	}
	public double getHra() {
		return hra;
	}
	public void setHra(double hra) {
		this.hra = hra;
	}

	@Override
	public double getSalary(int noOfDayWorked) {
		return 0;
	}

}
