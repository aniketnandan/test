package com.tcs;

public class Tester {
	public static void main(String[] args) {
		Company c = new Company("ABT");
		
		c.addEmployee(100, "Aghosh", "kolkata", 26000, 6000);
		c.addEmployee(101, "Anandan", "kolkata", 20000, 5000);
		
		c.addProject(10547, "Mongo", 5);
		c.addProject(12536, "Nasta", 6);

		try{
			c.addEmployeeToProject(10547, 101);
			c.addEmployeeToProject(12536, 100);	
		} catch(EmployeeNotFoundException e) {
			System.out.println(e.getStackTrace());
		}
		
		//System.out.println(c.addEmployeeToProject(10547, 101)); //it will show false
		c.addProject(10547, "Mongo", 5); // project is already exist
		c.addEmployee(100, "Aghosh", "kolkata", 26000, 6000); // employee already exist
	}

}
