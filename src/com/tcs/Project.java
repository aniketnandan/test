package com.tcs;
import java.util.ArrayList;

public class Project {
	private int projectId;
	private String projectName;
	private int duration;
	private ArrayList<Employee> empList;
	
	public Project(int projectId, String projectName, int duration) {
		this.empList = new ArrayList<Employee>();
		this.projectId = projectId;
		this.projectName = projectName;
		this.duration = duration;
	}
	public int getProjectId() {
		return projectId;
	}
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public ArrayList<Employee> getEmpList() {
		return empList;
	}
	public void setEmpList(ArrayList<Employee> empList) {
		this.empList = empList;
	}
	public void addEmployee(Employee e){
		empList.add(e);
	}
}
